import boto3

client = boto3.client('dynamodb')

def retrieve_answer(city, tag):
    """Access stored records in query_answers table."""
    key = {
        'city':{'S':city},
        'tag':{'S':tag}
    }
    table_name = 'query_answers'
    response = client.get_item(
        TableName = table_name, 
        Key = key
    )
    if response:
        return response.get('Item', None)
    else:
        return None

def save_answer(city, tag, feature_per_x_people):
    """Write new records to query_answers table."""
    # convert number to string because that's how DynamoDB likes it
    feature_per_x_people = str(feature_per_x_people)
    item = {
        'city':{'S':city},
        'tag':{'S':tag},
        'feature_per_x_people':{'N':feature_per_x_people}
    }
    table_name = 'query_answers'
    response = client.put_item(
        TableName = table_name, 
        Item = item
    )