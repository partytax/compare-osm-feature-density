#!/bin/bash
echo "Beginning packaging process."

echo "Beginning local install of Python requirements."
pip3 install -r requirements.txt -t .
echo "Python requirements local installation complete."

echo "Beginning zipping process."
zip -r package.zip ./*
echo "Zipping process complete."

echo "Please access the Lambda console in AWS and upload 'package.zip'."
