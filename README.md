# Compare OSM Feature Density

## About
This tool is a web app built on AWS Lambda and DynamoDB. It accepts the names of two cities and an OpenStreetMap tag, then returns per-capita information on how many of the given feature are present in each city. Here's a [demo](https://y5z77eigab.execute-api.us-east-2.amazonaws.com/app/).

![Screenshot of web app front page, featuring some gaudy ASCII art](readme-header.png)

## Prerequisites
* Computer running Linux or MacOS
* `bash`, `git`, and `python3` installed
* Amazon Web Services account
* Internet connection

## Setup

### Collect and Package Files
* Access a shell on your computer
* Move to an appropriate working directory and run `git clone https://gitlab.com/partytax/compare-osm-feature-density.git`
* Run `cd compare-osm-feature-density`
* Run `bash package-for-aws.sh`

### Create AWS Lambda Function
* Navigate to [https://console.aws.amazon.com/lambda](https://console.aws.amazon.com/lambda) and sign in if you haven't already.
* Click `Create function`.
* Select `Author from scratch`.
* Enter a name of your choosing into the `Function name` box.
* Select `Python 3.9` in runtime menu.
* Under `Change default execution role`, ensure `Execution role` is set to `Create a new role with basic Lambda permissions`.
* Click `Create function`.
* Click `Upload from` --> `.zip file` in `Code source` box.
* Click `Upload` and select `package.zip` from the `compare-osm-feature-density` folder.
* Click `Save`.
* In the horizontal tab collection towards the left of the page, click `Configuration`.
* On the right side of the `General configuration` box, click `Edit`.
* Under `Basic settings` --> `Timeout` --> `sec`, enter `30`.
* Click `Save`.

### Configure AWS role
* Navigate to [https://console.aws.amazon.com/iamv2](https://console.aws.amazon.com/iamv2).
* Under `Access management` click `Roles`.
* Click on the title of the role that was created when the Lambda function was created. It should be named according to the format [name of Lambda function]-role-[8 random characters].
* Click `Attach policies`.
* Check `PowerUserAccess` (a bit excessive, but this is a demo).

### Create DynamoDB Table
* Navigate to [https://console.aws.amazon.com/dynamodb](https://console.aws.amazon.com/dynamodb).
* Under `Create resources`, click `Create table`.
* Under `Table details`:
    * Under `Table name`, enter `query_answers`.
    * Under `Partition key`, enter `city`.
    * Under `Sort key - optional`, enter `tag`.
* Click `Create table`.

### Create API in AWS API Gateway
* Navigate to [https://console.aws.amazon.com/apigateway](https://console.aws.amazon.com/apigateway).
* In `REST API` box, click `Build`.
* Under `Choose the protocol`, select `REST`.
* Under `Create new API`, select `New API`.
* Under `Settings`:
    * Under `API name`, enter a name of your choosing.
    * Under `Endpoint Type`, select `Regional`.
* Click `Create API`.
* Under `Actions` menu near top of page, click `Create Method`.
* Under the blank menu that appears, select `GET` and click the checkmark.
* Under `/ - GET - Setup`:
    * Under `Integration type`, select `Lambda Function`.
    * Under `Use Lambda Proxy integration`, check the box.
    * Under `Lambda Function`, enter the name of the Lambda function created earlier.
    * Click `Save`.
* Under `Add Permission to Lambda Function`, click `OK`.
* Under `Actions` menu near top of page, click `Create Method`.
* Under the blank menu that appears, select `POST` and click the checkmark.
* Under `/ - POST - Setup`:
    * Under `Integration type`, select `Lambda Function`.
    * Under `Use Lambda Proxy integration`, check the box.
    * Under `Lambda Function`, enter the name of the Lambda function created earlier.
    * Click `Save`.
* Under `Add Permission to Lambda Function`, click `OK`.
* Under `Actions menu near top of page, click `Deploy API`.
* Under `Deploy API`:
    * Under `Deployment stage`, select `[New Stage]`
    * Under `Stage name`, enter `app`
    * Click `Deploy`.
* Under the `API: [name of API]` heading on left of page, click `Dashboard`.
* In blue box near top of page, click link after `Invoke this API at:`.

### Test App
* The app is ready to run. Have fun with it!
