import json
import requests
from exceptions import *


def get_pop_and_osm_id(
        city,
        endpoint="https://query.wikidata.org/bigdata/namespace/wdq/sparql"
):
    query = """
    SELECT ?item ?num ?population ?osm_id WHERE {{
        SERVICE wikibase:mwapi {{
            bd:serviceParam wikibase:api "EntitySearch".
            bd:serviceParam wikibase:endpoint "www.wikidata.org".
            bd:serviceParam wikibase:limit 10 .
            bd:serviceParam mwapi:search "{city}".
            bd:serviceParam mwapi:language "en".
            ?item wikibase:apiOutputItem mwapi:item.
            ?num wikibase:apiOrdinal true.
        }}
        
        ?item (wdt:P279|wdt:P31) ?type.
        ?item wdt:P1082 ?population.
        OPTIONAL {{?item wdt:P402 ?osm_id}}.
    }}
    ORDER BY ?searchTerm ?num
    """.format(city = city)
    
    # set custom user agent to avoid HTTP 403 responses
    # suggested by https://www.mediawiki.org/wiki/Topic:V1zau9rqd4ritpug
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    
    
    response = requests.get(
        endpoint, 
        params = {'query': query, 'format':'json'},
        headers = headers
    )
    
    print(f"Wikidata HTTP Status Code: {response.status_code}")
    print(f"Wikidata Response: {response}")
    
    json_data = response.json()

    try:
        population = json_data['results']['bindings'][0]['population']['value']
    except IndexError as e:
        print(f'Unable to fetch city details due to {repr(e)}')
        raise CityDetailsError
    except KeyError as e:
        print(f'Unable to fetch city details due to {repr(e)}')
        raise CityDetailsError
    
    try:
        osm_id = json_data['results']['bindings'][0]['osm_id']['value']
    except IndexError as e:
        osm_id = None
    except KeyError as e:
        osm_id = None
        
    
    return {
        'population': population,
        'osm_id': osm_id
    }
