import overpy
from geopy.geocoders import Nominatim
from exceptions import *



def osm_id_to_over_id(osm_id):
    """Convert OSM ID to Overpass ID by adding 3,600,000,000."""
    overpass_id = int(osm_id) + 3600000000
    
    return overpass_id


def geocode_boundary(place_name):
    """Get OSM relation ID that corresponds to a place name."""
    geolocator = Nominatim(user_agent="compare_osm_feature_density")
    geo_results = geolocator.geocode(place_name, exactly_one=False)
    for result in geo_results:
        if result.raw.get("osm_type") == "relation":
            place_relation = result
            break
    if "place_relation" in locals():
        osm_id = place_relation.raw.get("osm_id")
        return osm_id
    else:
        return None


def geocode_within_boundary(overpass_id, tag_list, timeout=60):
    """Get overpy object of OSM features with given tag in Overpass relation."""
    try:
        api = overpy.Overpass()
        query_body = ""
        for tag in tag_list:
            # if tag contains spaces, quote it so that Overpass API is not confused
            if " " in tag:
                tag = "\'" + tag + "\'"
                
            query_body += f"node[{tag}](area.searchArea); way[{tag}](area.searchArea); relation[{tag}](area.searchArea);"
        query = f"[out:json][timeout:{timeout}]; area({overpass_id})->.searchArea;({query_body}); out center qt; >;"
        results = api.query(query)
        
        return results
    
    except Exception as e:
        raise FeatureQueryError


def count_overpass_elements(overpass_results):
    """Count discrete features within overpy object."""
    node_count = len(overpass_results.nodes)
    way_count = len(overpass_results.ways)
    relation_count = len(overpass_results.relations)
    
    total_count = node_count + way_count + relation_count
    
    return total_count
    
    
def query_elements_in_city(name, tag_string, osm_id=None):
    """Get features with given OSM tag in given city."""
    try:
        if osm_id:
            elements = geocode_within_boundary(osm_id_to_over_id(osm_id), [tag_string])
        else:
            elements = geocode_within_boundary(osm_id_to_over_id(geocode_boundary(name)), [tag_string])
    except Exception as e:
        print(f'Unable to query features in city due to {repr(e)}')
        raise FeatureQueryError
        
    return elements