class FeatureQueryError(Exception):
    pass

class CityDetailsError(Exception):
    pass

class FormEmptyError(Exception):
    pass