import json
import openstreetmap as osm
import wikidata as wd
import database as db
from urllib import parse as urlparse
from exceptions import *
from overpy.exception import OverpassTooManyRequests


def lambda_handler(event, context):
    """Handle and respond to incoming API requests."""
    page = page_router(
        event['httpMethod'],
        event['queryStringParameters'], 
        event['body']
    )
    
    return page

def page_router(http_method, query_string, form_body):
    """Accept input from API and return form, results, or error page."""
    if http_method == 'GET':
        html_file = open('form.html', 'r')
        html_content = html_file.read()
        return {
            'statusCode': 200,
            'headers': {'Content-Type': 'text/html'},
            'body': html_content
        }
    if http_method == 'POST':
        try:
            results = gather_and_process_data(form_body)
        except CityDetailsError as e:
            html_file = open('error.html', 'r')
            html_content = html_file.read().format(
                friendly_error = "Unable to get city population from \
                    Wikidata."
            )
            return {
                'statusCode': 500,
                'headers': {'Content-Type': 'text/html'},
                'body': html_content
            }
        except FeatureQueryError as e:
            html_file = open('error.html', 'r')
            html_content = html_file.read().format(
                friendly_error = "Unable to get feature count from \
                    OpenStreetMap."
            )
            return {
                'statusCode': 500,
                'headers': {'Content-Type': 'text/html'},
                'body': html_content
            }
        except OverpassTooManyRequests as e:
            html_file = open('error.html', 'r')
            html_content = html_file.read().format(
                friendly_error = "Unable to get feature count from \
                    OpenStreetMap due to Overpass query service being \
                    overloaded. Service should be available again within 30 \
                    seconds."
            )
            return {
                'statusCode': 500,
                'headers': {'Content-Type': 'text/html'},
                'body': html_content
            }
        except ZeroDivisionError as e:
            html_file = open('error.html', 'r')
            html_content = html_file.read().format(
                friendly_error = "Unable to calculate density of feature \
                because one or more of the cities has 0 of that feature."
            )
            return {
                'statusCode': 500,
                'headers': {'Content-Type': 'text/html'},
                'body': html_content
            }
        except FormEmptyError as e:
            html_file = open('error.html', 'r')
            html_content = html_file.read().format(
                friendly_error = "Unable to return results because form is \
                empty."
            )
            return {
                'statusCode': 500,
                'headers': {'Content-Type': 'text/html'},
                'body': html_content
            }
        else:    
            html_file = open('results.html', 'r')
            html_content = html_file.read().format(
                city1_name = results[0]['name'],
                city1_feature_per_x_people = results[0]['feature_per_x_people'],
                city2_name = results[1]['name'],
                city2_feature_per_x_people = results[1]['feature_per_x_people'],
                tag = results[0]['tag']
            )
    
            return {
                'statusCode': 200,
                'headers': {'Content-Type': 'text/html'},
                'body': html_content
            }
        

def gather_and_process_data(form_body):
    """Gather population & feature count for each city and calculate density."""
    parsed_form_body = urlparse.parse_qs(form_body)
    
    if len(parsed_form_body.get('cities', [])) == 2:
        both_cities_present = True
    else:
        both_cities_present = False
    
    if len(parsed_form_body.get('tag', [])) == 1:
        tag_present = True
    else:
        tag_present = False
    
    if not both_cities_present or not tag_present:
        raise FormEmptyError
    
    # extract tag from parsed form body that is stuck in a one-item list
    tag = parsed_form_body['tag'][0]
    
    # iterate through city names from form and gather data on each
    results = []
    
    for city in parsed_form_body['cities']:
        db_response = db.retrieve_answer(city, tag)
        if not db_response:
            # gather data from Wikidata
            try:
                wikidata_results = wd.get_pop_and_osm_id(city)
            except CityDetailsError as e:
                raise
            else: 
                population = int(wikidata_results['population'])
            
            # gather data from OpenStreetMap
            try:
                elements = osm.query_elements_in_city(city, tag, wikidata_results['osm_id'])
            except FeatureQueryError as e:
                raise
            except OverpassTooManyRequests as e:
                raise
            else:
                element_count = osm.count_overpass_elements(elements)
            
            # calculate feature density based on population and element count
            try:
                feature_per_x_people = round(population / element_count)
            except ZeroDivisionError as e:
                raise
            
            db.save_answer(city, tag, feature_per_x_people)
            
        else:
            feature_per_x_people = int(
                db_response['feature_per_x_people']['N']
            )
        details = {
            'name': city,
            'tag': tag,
            'feature_per_x_people': feature_per_x_people
        }
        
        results.append(details)
    
    return results